defmodule Grains.RecipeTest do
  use ExUnit.Case

  alias Grains.Recipe

  test "to_mermaid/1,2" do
    assert "graph TD" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid()

    assert "flowchart TD" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid(type: "flowchart")

    assert "graph TB" ==
             Test
             |> Recipe.new(%{})
             |> Recipe.to_mermaid(direction: "TB")
  end
end
