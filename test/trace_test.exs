defmodule Grains.TraceTest do
  use ExUnit.Case

  alias Grains.Recipe
  alias Grains.Supervisor
  alias Grains.GenGrain
  alias Grains.Support.Utils
  alias Grains.Support.Trace

  doctest Grains.Support.Trace

  defmodule TestGrain do
    use GenGrain

    def init(tag) do
      {:ok, tag}
    end

    def handle_info(:pull_please, tag) do
      pull()
      {:noreply, tag}
    end

    def handle_push(msg, _from, tag) do
      push([tag | msg])
      {:noreply, tag}
    end

    def handle_pull(_msg, tag) do
      {:noreply, tag}
    end
  end

  def start_grains_sup(recipe, grains, args \\ []) do
    spec = %{id: :test_grains_sup, start: {Supervisor, :start_link, [recipe, grains, args]}}
    start_supervised(spec)
  end

  test "trace push receive" do
    recipe = Recipe.new(:test1, %{})
    grains = Grains.new(%{A => {TestGrain, A, []}})

    {:ok, sup} = start_grains_sup(recipe, grains)
    start_supervised!(Trace)

    Trace.push(sup, target: A, what: :receive)
    Utils.inject_push(sup, A, Test, [:foo])
    assert_receive {:receive, A, {:push, Test, [:foo]}}
    Utils.inject_push(sup, A, Test, [:bar])
    assert_receive {:receive, A, {:push, Test, [:bar]}}

    Trace.stop_push(sup, target: A, what: :receive)

    Utils.inject_push(sup, A, Test, [:bar])
    refute_receive {:receive, A, {:push, Test, [:bar]}}
  end

  test "trace push send" do
    recipe = Recipe.new(:test1, %{A => B})

    grains =
      Grains.new(%{
        A => {Grains.Support.Publisher, [], []},
        B => {TestGrain, B, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)
    start_supervised!(Trace)

    a = Grains.get_name(sup, A)

    Trace.push(sup, target: A, what: :send)
    GenServer.cast(a, :foo)
    assert_receive {:send, A, B, {:push, A, :foo}}

    Trace.stop_push(sup, target: A, what: :send)
    GenServer.cast(a, :foo)
    refute_receive {:send, A, B, {:push, A, :foo}}
  end

  test "trace pull receive" do
    recipe = Recipe.new(:test1, %{A => B})

    grains =
      Grains.new(%{
        A => {TestGrain, A, []},
        B => {TestGrain, B, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)
    start_supervised!(Trace)

    Trace.pull(sup, target: A, what: :receive)
    Utils.inject_pull_all(sup, B)
    assert_receive {:receive, A, {:pull, B}}

    Trace.stop_pull(sup, target: A, what: :receive)
    Utils.inject_pull_all(sup, B)
    refute_receive {:receive, A, {:pull, B}}
  end

  test "trace pull send" do
    recipe = Recipe.new(:test1, %{A => B, C => B})

    grains =
      Grains.new(%{
        A => {TestGrain, A, []},
        B => {TestGrain, B, []},
        C => {TestGrain, C, []}
      })

    {:ok, sup} = start_grains_sup(recipe, grains)
    b = Grains.get_name(sup, B)
    start_supervised!(Trace)

    Trace.pull(sup, target: B, what: :send)
    send(b, :pull_please)
    assert_receive {:send, B, A, {:pull, B}}
    assert_receive {:send, B, C, {:pull, B}}

    Trace.stop_pull(sup, target: B, what: :send)
    refute_receive {:send, B, A, {:pull, B}}
  end
end
